package util;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by raymondlau on 4/22/14.
 */
public final class RandomString {

    private static SecureRandom random = new SecureRandom();

    public static String getRandomString() {
        return new BigInteger(130, random).toString(32);
    }
}
