package util;

import play.Logger;
import play.Play;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public final class Postgres {

    private static String url = Play.application().configuration().getString("db_url");
    private static Connection connection = null;

    public static Connection connect() throws SQLException {
        return DriverManager.getConnection(url);
    }

    public static Connection get() {
        try {
            return DriverManager.getConnection(url);
        } catch (SQLException ex) {
            Logger.error(ex.getMessage());
            return null;
        }
    }
}
