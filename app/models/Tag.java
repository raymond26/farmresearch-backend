package models;

import play.db.ebean.Model;

public class Tag extends Model {

    public String tag;

    public Long o_id;

    public Long p_id;

    public static Finder<String, Tag> tagFiner = new Finder<String, Tag>(
            String.class, Tag.class
    );
}
