package models;

import play.db.ebean.Model;
import javax.persistence.*;
import java.util.List;

@Entity
public class ObservationPhoto extends Model {

    @Id
    public Long id;

    public String filename;

    public String location_on_disk;

    //the observation it belongs to
    public Long o_id;

    public static Finder<Long, ObservationPhoto> photoFinder = new Finder<Long, ObservationPhoto>(
            Long.class, ObservationPhoto.class
    );

    public static ObservationPhoto get(Long id) {
        return photoFinder.byId(id);
    }

    public static List<ObservationPhoto> getByObservation(Long id) {
        return photoFinder.where().eq("o_id", id).findList();
    }

}
