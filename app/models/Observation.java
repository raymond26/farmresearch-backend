package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.joda.time.DateTime;

import play.db.ebean.*;
import play.data.validation.Constraints.*;

import java.util.List;
import java.util.Set;
import javax.persistence.*;

@Entity
public class Observation extends Model {

    @Id
    public Long id;

    public DateTime observed_on;

    @Required
    public DateTime posted_on;

    public String description;

    public Set<String> tags;

    public long latitude;

    public long longitude;

    @Required
    public int plot_number;

    public int bed_number;

    public String getObserved_on() {
        return observed_on.toString("MM/dd/yyyy hh:mm");
    }

    public String getPosted_on() {
        return posted_on.toString("MM/dd/yyyy hh:mm");
    }

    public static Finder<Long, Observation> observationFinder = new Finder<Long, Observation>(
            Long.class, Observation.class
    );

    public static List<Observation> all() {
        return observationFinder.all();
    }

    public static void create(Observation observation) {
        observation.save();
    }

    public static Observation get(Long id) {
        return observationFinder.byId(id);
    }

    public static void delete(Long id) {
        observationFinder.ref(id).delete();
    }
}
