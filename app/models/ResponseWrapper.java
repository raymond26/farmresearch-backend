package models;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

public class ResponseWrapper {

    public static ObjectNode badRequest(String error) {
        ObjectNode obj = Json.newObject();
        obj.put("error", error);
        obj.put("code", 400);
        return obj;
    }

    public static ObjectNode success() {
        ObjectNode obj = Json.newObject();
        obj.put("code", 200);
        return obj;
    }

    public static ObjectNode unauthorized() {
        ObjectNode obj = Json.newObject();
        obj.put("error", "User is unauthorized.  Wrong password.");
        obj.put("code", 401);
        return obj;
    }

    public static ObjectNode unauthorized(String error) {
        ObjectNode obj = Json.newObject();
        obj.put("error", error);
        obj.put("code", 401);
        return obj;
    }

    public static ObjectNode notFound(String error) {
        ObjectNode obj = Json.newObject();
        obj.put("error", error);
        obj.put("code", 404);
        return obj;
    }
}
