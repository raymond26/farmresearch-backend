package models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.joda.time.DateTime;
import play.data.validation.Constraints;
import play.db.ebean.Model;

import javax.persistence.*;

@Entity
public class Member extends Model {

    @Id
    @Constraints.Required
    public String username;

    @Constraints.Required
    public String password; //hashed

    @Constraints.Required
    @JsonIgnore
    public DateTime joined_on;

    @JsonIgnore
    public DateTime last_login;

    @Constraints.Required
    public String email_address;

    public static Finder<String, Member> memberFinder = new Finder<String, Member>(
            String.class, Member.class
    );

    public static void create(Member member) {
        member.save();
    }

    public static Member get(String username) {
        return memberFinder.byId(username);
    }
}
