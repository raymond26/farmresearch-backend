package models;

/**
 * Created by raymondlau on 5/8/14.
 */
public enum MemberType {

    NORMAL((short)0),
    UCDAVIS_FACULTY((short)1),
    UCDAVIS_NORMAL((short)2);

    private short code;
    private MemberType(short code) {
        this.code = code;
    }

    public short getCode() {
        return code;
    }
}
