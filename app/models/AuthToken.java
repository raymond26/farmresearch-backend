package models;

import org.joda.time.DateTime;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import javax.persistence.*;

@Entity
public class AuthToken extends Model {

    @Id
    @Constraints.Required
    public String token;

    @Constraints.Required
    public String username;

    @Constraints.Required
    public DateTime issued_on;

    @Constraints.Required
    public DateTime expires_on;

    @Constraints.Required
    public static Finder<String, AuthToken> authTokenFinder = new Finder<String, AuthToken>(
            String.class, AuthToken.class
    );

    public static AuthToken get(String token) {
        return authTokenFinder.byId(token);
    }

    public static void create(AuthToken token) {
        token.save();
    }
}
