package controllers;

import models.ResponseWrapper;
import play.Logger;
import play.libs.F;
import play.mvc.*;
import util.Postgres;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Created by raymondlau on 5/1/14.
 */
public class SecurityController extends Action.Simple {

    public final static String AUTH_TOKEN_HEADER = "X-AUTH-TOKEN";
    public static final String AUTH_TOKEN = "authToken";

    public F.Promise<SimpleResult> call(Http.Context ctx) throws Throwable {
        String[] authTokenHeaderValues = ctx.request().headers().get(AUTH_TOKEN_HEADER);
        if ((authTokenHeaderValues != null) && (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)) {
            //user = models.User.findByAuthToken(authTokenHeaderValues[0]);
            Connection c = Postgres.get();
            Statement st = c.createStatement();
            ResultSet rs = st.executeQuery("SELECT token, user_id FROM auth_tokens WHERE token = '" + authTokenHeaderValues[0] + "'");
            int rowCount = 0;
            if(rs.next()) { rowCount += 1; }
            if (rowCount > 0) {
                ctx.args.put("user_id", rs.getInt("user_id"));
                return delegate.call(ctx);
            } else {
                return F.Promise.pure((SimpleResult)unauthorized(ResponseWrapper.unauthorized("Invalid auth token")));
            }
        }
        return F.Promise.pure((SimpleResult)unauthorized(ResponseWrapper.unauthorized("Please send a valid auth token under the X-AUTH-TOKEN header.  Do not send more than 1")));
        //return delegate.call(ctx);
    }
}
