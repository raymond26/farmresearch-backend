package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.ResponseWrapper;
import org.joda.time.DateTime;
import play.Logger;
import play.libs.Json;
import play.mvc.*;
import util.Postgres;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ObservationTagsController extends Controller {

    public static Result tagObservation(Integer id) throws SQLException {
        Connection c = Postgres.get();

        Statement check_st = c.createStatement();
        ResultSet rs = check_st.executeQuery("SELECT * FROM observations WHERE id = " + id);

        int rs_size = 0;
        if(rs.next()) {
            rs_size++;
        }
        if(rs_size < 1) {
            return notFound(ResponseWrapper.notFound("Observation with that ID does not exist."));
        }

        JsonNode tags = request().body().asJson();
        List<String> values = new ArrayList<String>();
        for(JsonNode tag : tags.get("tags")) {
            String value = "(" + id + ",'" + tag.asText().replace("'","''") + "',1," + DateTime.now().getMillis() + ")";
            values.add(value);
        }
        String insertStatement = "INSERT INTO observation_tags VALUES " + org.apache.commons.lang3.StringUtils.join(values,",");
        Statement st = c.createStatement();
        st.executeUpdate(insertStatement);
        c.close();

        return ok(ResponseWrapper.success());
    }

    public static Result getObservationsByTag(String tag) throws SQLException {
        Connection c = Postgres.get();
        PreparedStatement st = c.prepareStatement(
          "SELECT id, user_id, observed_on, posted_on, description, latitude, longitude, plot_number, plot_subnumber, bed_number " +
          "FROM observations INNER JOIN observation_tags ON observations.id = observation_tags.observation_id " +
          "WHERE observation_tags.tag = ?");
        st.setString(1, tag);
        ResultSet rs = st.executeQuery();
        c.close();
        List<ObjectNode> response = new ArrayList<ObjectNode>();
        while(rs.next()) {
            ObjectNode node = Json.newObject();
            node.put("id", rs.getInt("id"));
            node.put("user_id", rs.getInt("user_id"));
            node.put("observed_on", rs.getLong("observed_on"));
            node.put("posted_on", rs.getLong("posted_on"));
            node.put("description", rs.getString("description"));
            node.put("latitude", rs.getDouble("latitude"));
            node.put("longitude", rs.getDouble("longitude"));
            node.put("plot_number", rs.getInt("plot_number"));
            node.put("plot_subnumber", rs.getInt("plot_subnumber"));
            node.put("bed_number", rs.getInt("bed_number"));
            response.add(node);
        }
        ObjectNode wrapper = Json.newObject();
        wrapper.put("observations", Json.toJson(response));
        return ok(wrapper);
    }

    public static Result getObservationTags(Integer id) throws SQLException {
        Connection c = Postgres.get();
        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery("SELECT tag FROM observation_tags WHERE observation_id = " + id);
        c.close();
        return ok();
    }
}
