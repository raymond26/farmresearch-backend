package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Observation;
import models.ResponseWrapper;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import play.Logger;
import play.Play;
import play.libs.Json;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.*;
import play.mvc.Result;
import util.Postgres;
import util.RandomString;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ObservationsController extends Controller {

    public static Result queryObservations() throws SQLException {
        JsonNode json = request().body().asJson();
        Connection c = Postgres.get();
        Statement st = c.createStatement();
        boolean isFirst = true;
        String queryString = "SELECT * FROM observations WHERE ";

        if(json.get("plot_number") != null) {
            if(isFirst)
                isFirst = false;
            else
                queryString = queryString.concat(" and ");
            queryString = queryString.concat("plot_number = " + json.get("plot_number").asInt());
        }
        if(json.get("plot_subnumber") != null) {
            if(isFirst)
                isFirst = false;
            else
                queryString = queryString.concat(" and ");
            queryString = queryString.concat("plot_subnumber = " + json.get("plot_subnumber").asInt());
        }
        if(json.get("bed_number") != null) {
            if(isFirst)
                isFirst = false;
            else
                queryString = queryString.concat(" and ");
            queryString = queryString.concat("bed_number = " + json.get("bed_number").asInt());
        }
        if(json.get("latitude_from") != null) {
            if(isFirst)
                isFirst = false;
            else
                queryString = queryString.concat(" and ");
            queryString = queryString.concat("latitude >= " + json.get("latitude_from").asDouble());
        }
        if(json.get("latitude_to") != null) {
            if(isFirst)
                isFirst = false;
            else
                queryString = queryString.concat(" and ");
            queryString = queryString.concat("latitude <= " + json.get("latitude_to").asDouble());
        }
        if(json.get("longitude_from") != null) {
            if(isFirst)
                isFirst = false;
            else
                queryString = queryString.concat(" and ");
            queryString = queryString.concat("longitude >= " + json.get("longitude_from").asDouble());
        }
        if(json.get("longitude_to") != null) {
            if(isFirst)
                isFirst = false;
            else
                queryString = queryString.concat(" and ");
            queryString = queryString.concat("longitude <= " + json.get("longitude_to").asDouble());
        }
        if(json.get("observed_from") != null) {
            if(isFirst)
                isFirst = false;
            else
                queryString = queryString.concat(" and ");
            queryString = queryString.concat("observed_on >= " + json.get("observed_from").asLong());
        }
        if(json.get("observed_to") != null) {
            if(isFirst)
                isFirst = false;
            else
                queryString = queryString.concat(" and ");
            queryString = queryString.concat("observed_on <= " + json.get("observed_to").asLong());
        }

        ResultSet rs = st.executeQuery(queryString);
        c.close();
        List<ObjectNode> response = new ArrayList<ObjectNode>();
        while(rs.next()) {
            ObjectNode node = Json.newObject();
            node.put("id", rs.getInt("id"));
            node.put("user_id", rs.getInt("user_id"));
            node.put("observed_on", rs.getLong("observed_on"));
            node.put("posted_on", rs.getLong("posted_on"));
            node.put("description", rs.getString("description"));
            node.put("latitude", rs.getDouble("latitude"));
            node.put("longitude", rs.getDouble("longitude"));
            node.put("plot_number", rs.getInt("plot_number"));
            node.put("plot_subnumber", rs.getInt("plot_subnumber"));
            node.put("bed_number", rs.getInt("bed_number"));
            response.add(node);
        }
        ObjectNode wrapper = Json.newObject();
        wrapper.put("observations", Json.toJson(response));
        return ok(wrapper);
    }

    @With(SecurityController.class)
    public static Result addObservation() throws SQLException {
        JsonNode json = request().body().asJson();
        Connection c = Postgres.get();
        PreparedStatement st = c.prepareStatement(
                "INSERT INTO observations " +
                        "(user_id, observed_on, posted_on, description, latitude, longitude, plot_number, bed_number, plot_subnumber) " +
                        "VALUES (?,?,?,?,?,?,?,?,?) RETURNING id"
        );
        Integer user_id = null;
        if(Http.Context.current().args.get("user_id") != null) {
            user_id = (Integer)Http.Context.current().args.get("user_id");
        } else {
            user_id = 1;
        }
        st.setLong(1,user_id);
        if(json.get("observed_on") != null) {
            st.setLong(2, json.get("observed_on").asLong());
        }
        st.setLong(3,DateTime.now().getMillis());
        if(json.get("description") != null) {
            st.setString(4, json.get("description").asText());
        }
        if(json.get("latitude") != null) {
            st.setDouble(5, json.get("latitude").asDouble());
        } else {
            st.setDouble(5,0);
        }
        if(json.get("longitude") != null) {
            st.setDouble(6, json.get("longitude").asDouble());
        } else {
            st.setDouble(6,0);
        }
        if(json.get("plot_number") != null) {
            if(json.get("plot_number").asInt() < 1 || json.get("plot_number").asInt() > 72) {
                return badRequest(Json.newObject().put("error", "Plot number not in acceptable range.  1-72"));
            } else {
                st.setInt(7,json.get("plot_number").asInt());
            }
        } else {
            return badRequest(Json.newObject().put("error", "Plot number is required."));
        }
        if(json.get("bed_number") != null) {
            if(json.get("bed_number").asInt() < 1 || json.get("bed_number").asInt() > 42) {
                return badRequest(Json.newObject().put("error", "Bed number not in acceptable range.  1-42"));
            } else {
                st.setInt(8,json.get("bed_number").asInt());
            }
        } else {
            st.setInt(8, 0);
        }
        if(json.get("plot_subnumber") != null) {
            st.setInt(9, json.get("plot_subnumber").asInt());
        } else {
            st.setInt(9, 0);
        }
        ResultSet rs = st.executeQuery();
        rs.next();
        c.close();
        ObjectNode response = Json.newObject();
        response.put("id", rs.getInt("id"));
        return ok(response);
    }

    @With(SecurityController.class)
    public static Result editObservation(Integer id) throws SQLException {
        Integer user_id = null;
        if(Http.Context.current().args.get("user_id") != null) {
            user_id = (Integer)Http.Context.current().args.get("user_id");
        } else {
            user_id = 1;
        }
        Connection c = Postgres.get();
        Statement check_st = c.createStatement();
        ResultSet rs = check_st.executeQuery("SELECT user_id FROM observations WHERE id = " + id);
        if(rs.next()) {
            Integer o_user_id = rs.getInt("user_id");
            if(!user_id.equals(o_user_id)) {
                return unauthorized(ResponseWrapper.unauthorized("You do not own that resource."));
            }
        } else {
            return notFound(ResponseWrapper.notFound("Observation does not exist."));
        }

        JsonNode json = request().body().asJson();
        int parameter_ct = 0;
        if(json.get("observed_on") != null) { parameter_ct += 1; }
        if(json.get("description") != null) { parameter_ct += 1; }
        if(json.get("latitude") != null) { parameter_ct += 1; }
        if(json.get("longitude") != null) { parameter_ct += 1; }
        if(json.get("plot_number") != null) { parameter_ct += 1; }
        if(json.get("bed_number") != null) { parameter_ct += 1; }
        if(json.get("plot_subnumber") != null) { parameter_ct += 1; }

        String update_st = "UPDATE observations SET ";

        int index = 0;
        if(json.get("observed_on") != null) {
            index += 1;
            update_st = update_st.concat("observed_on = ?");
            if(index < parameter_ct) { update_st = update_st.concat(","); }
        }
        if(json.get("description") != null) {
            index += 1;
            update_st = update_st.concat("description = ?");
            if(index < parameter_ct) { update_st = update_st.concat(","); }
        }
        if(json.get("latitude") != null) {
            index += 1;
            update_st = update_st.concat("latitude = ?");
            if(index < parameter_ct) { update_st = update_st.concat(","); }
        }
        if(json.get("longitude") != null) {
            index += 1;
            update_st = update_st.concat("longitude = ?");
            if(index < parameter_ct) { update_st = update_st.concat(","); }
        }
        if(json.get("plot_number") != null) {
            if(json.get("plot_number").asInt() < 1 || json.get("plot_number").asInt() > 72) {
                return badRequest(Json.newObject().put("error", "Plot number not in acceptable range.  1-72"));
            }
            index += 1;
            update_st = update_st.concat("plot_number = ?");
            if(index < parameter_ct) { update_st = update_st.concat(","); }
        }
        if(json.get("bed_number") != null) {
            if(json.get("bed_number").asInt() < 1 || json.get("bed_number").asInt() > 42) {
                return badRequest(Json.newObject().put("error", "Bed number not in acceptable range.  1-42"));
            }
            index += 1;
            update_st = update_st.concat("bed_number = ?");
            if(index < parameter_ct) { update_st = update_st.concat(","); }
        }
        if(json.get("plot_subnumber") != null) {
            index += 1;
            update_st = update_st.concat("plot_subnumber = ?");
            if(index < parameter_ct) { update_st = update_st.concat(","); }
        }
        update_st = update_st.concat(" WHERE id = " + id);

        PreparedStatement st = c.prepareStatement(update_st);
        int p_index = 1;
        if(json.get("observed_on") != null) {
            st.setLong(p_index, json.get("observed_on").asLong());
            p_index += 1;
        }
        if(json.get("description") != null) {
            st.setString(p_index, json.get("description").asText());
            p_index += 1;
        }
        if(json.get("latitude") != null) {
            st.setDouble(p_index, json.get("latitude").asDouble());
            p_index += 1;
        }
        if(json.get("longitude") != null) {
            st.setDouble(p_index, json.get("longitude").asDouble());
            p_index += 1;
        }
        if(json.get("plot_number") != null) {
            st.setInt(p_index, json.get("plot_number").asInt());
            p_index += 1;
        }
        if(json.get("bed_number") != null) {
            st.setInt(p_index, json.get("bed_number").asInt());
            p_index += 1;
        }
        if(json.get("plot_subnumber") != null) {
            st.setInt(p_index, json.get("plot_subnumber").asInt());
            p_index += 1;
        }
        //Logger.debug(st.toString());
        st.executeUpdate();
        return ok(ResponseWrapper.success());
    }

    /* File uploads are handled via multiform type so we can't do
    both JSON and files at the same time.  Separated
     */
    //@BodyParser.Of(BodyParser.MultipartFormData.class)
    @With(SecurityController.class)
    public static Result uploadObservationPhoto(Integer id) throws SQLException {
        if(request().body().asMultipartFormData() == null) {
            return badRequest(ResponseWrapper.badRequest("Did not receive valid multipart/form data"));
        }
        Connection c = Postgres.get();

        Statement check_st = c.createStatement();
        ResultSet rs = check_st.executeQuery("SELECT * FROM observations WHERE id = " + id);

        int rs_size = 0;
        if(rs.next()) {
            rs_size++;
        }
        if(rs_size < 1) {
            return notFound(ResponseWrapper.notFound("Observation with that ID does not exist."));
        }

        FilePart photo = request().body().asMultipartFormData().getFiles().get(0);
        String contentType = photo.getContentType();
        File directory = new File(Play.application().configuration().getString("user_media.directory"));
        String contentTypeSmall = null;
        if(contentType.equals("image/jpeg")) contentTypeSmall = "jpeg";
        else if(contentType.equals("image/gif")) contentTypeSmall = "gif";
        else if(contentType.equals("image/png")) contentTypeSmall = "png";
        if(contentTypeSmall == null) {
            return badRequest(ResponseWrapper.badRequest("Invalid image type.  Please use either a jpeg, gif, png image file."));
        }

        String newFilename = RandomString.getRandomString();
        String fullFilename = newFilename + "." + contentTypeSmall;

        if(!photo.getFile().renameTo(new File(directory, fullFilename))) {
            Logger.debug("Failed to move file");
        }
        Statement st = c.createStatement();
        Integer user_id = null;
        if(Http.Context.current().args.get("user_id") != null) {
            user_id = (Integer)Http.Context.current().args.get("user_id");
        } else {
            user_id = 1;
        }
        st.executeUpdate(
          "INSERT INTO observation_photos (filename, observation_id, uploaded_on, user_id) " +
          "VALUES ('" + fullFilename + "'," + id + "," + DateTime.now().getMillis() + "," + user_id + ")");
        c.close();

        ObjectNode node = Json.newObject();
        node.put("filename", fullFilename);
        return ok(node);
    }

    public static Result getObservationPhotos(Integer id) throws SQLException {
        Connection c = Postgres.get();
        Statement check_st = c.createStatement();
        ResultSet check_rs = check_st.executeQuery("SELECT * FROM observations WHERE id = " + id);

        int rs_size = 0;
        if(check_rs.next()) {
            rs_size++;
        }
        if(rs_size < 1) {
            return notFound(ResponseWrapper.notFound("Observation with that ID does not exist."));
        }

        Statement st = c.createStatement();

        ResultSet rs = st.executeQuery(
          "SELECT id, filename, location_on_disk, description, uploaded_on, user_id " +
          "FROM observation_photos WHERE observation_id = " + id
        );
        c.close();
        List<ObjectNode> response = new ArrayList<ObjectNode>();
        while(rs.next()) {
            ObjectNode node = Json.newObject();
            node.put("id", rs.getInt("id"));
            node.put("filename", rs.getString("filename"));
            node.put("location_on_disk", rs.getString("location_on_disk"));
            node.put("description", rs.getString("description"));
            node.put("uploaded_on", rs.getLong("uploaded_on"));
            node.put("user_id", rs.getInt("user_id"));
            response.add(node);
        }

        ObjectNode wrapper = Json.newObject();
        wrapper.put("photos", Json.toJson(response));
        return ok(wrapper);
    }

    @With(SecurityController.class)
    public static Result deletePhotoByID(Integer photo_id) throws SQLException {
        Connection c = Postgres.get();
        Statement st = c.createStatement();
        Integer delete_count = st.executeUpdate("DELETE FROM observation_photos WHERE id = " + photo_id);
        c.close();
        if(delete_count != 1 && delete_count < 1) {
            return notFound(ResponseWrapper.notFound("That photo was not found.  It was not deleted"));
        } else {
            return ok(ResponseWrapper.success());
        }
    }

    public static Result getAll() throws SQLException {
        Connection c = Postgres.get();
        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM observations");
        c.close();
        List<ObjectNode> response = new ArrayList<ObjectNode>();
        while(rs.next()) {
            ObjectNode node = Json.newObject();
            node.put("id", rs.getInt("id"));
            node.put("user_id", rs.getInt("user_id"));
            node.put("observed_on", rs.getLong("observed_on"));
            node.put("posted_on", rs.getLong("posted_on"));
            node.put("description", rs.getString("description"));
            node.put("latitude", rs.getDouble("latitude"));
            node.put("longitude", rs.getDouble("longitude"));
            node.put("plot_number", rs.getInt("plot_number"));
            node.put("plot_subnumber", rs.getInt("plot_subnumber"));
            node.put("bed_number", rs.getInt("bed_number"));
            response.add(node);
        }
        ObjectNode wrapper = Json.newObject();
        wrapper.put("observations", Json.toJson(response));
        return ok(wrapper);
    }

    public static Result getByID(Integer id) throws SQLException {
        Connection c = Postgres.get();

        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM observations WHERE id = " + id);

        int rs_size = 0;
        if(rs.next()) {
            rs_size++;
        }
        if(rs_size < 1) {
            return notFound(ResponseWrapper.notFound("Observation with that ID does not exist."));
        }

        Statement st2 = c.createStatement();
        ResultSet rs2 = st2.executeQuery("SELECT tag FROM observation_tags WHERE observation_id = " + id);
        c.close();
        List<String> tags = new ArrayList<String>();
        while(rs2.next()) {
            tags.add(rs2.getString("tag"));
        }
        List<ObjectNode> response = new ArrayList<ObjectNode>(1);
        ObjectNode node = Json.newObject();
        node.put("id", rs.getInt("id"));
        node.put("user_id", rs.getInt("user_id"));
        node.put("observed_on", rs.getLong("observed_on"));
        node.put("posted_on", rs.getLong("posted_on"));
        node.put("description", rs.getString("description"));
        node.put("latitude", rs.getDouble("latitude"));
        node.put("longitude", rs.getDouble("longitude"));
        node.put("plot_number", rs.getInt("plot_number"));
        node.put("plot_subnumber", rs.getInt("plot_subnumber"));
        node.put("bed_number", rs.getInt("bed_number"));
        node.put("tags", Json.toJson(tags));
        response.add(node);
        ObjectNode wrapper = Json.newObject();
        wrapper.put("observations", Json.toJson(response));
        return ok(node);
    }

    public static Result getByUser(String username) throws SQLException {
        Connection c = Postgres.get();
        PreparedStatement check_st = c.prepareStatement(
          "SELECT id FROM members WHERE username = ?"
        );
        check_st.setString(1, username);
        ResultSet check_rs = check_st.executeQuery();

        Integer user_id = null;
        if(!check_rs.next()) {
           return notFound(ResponseWrapper.notFound("User not found."));
        } else {
           user_id = check_rs.getInt("id");
        }

        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM observations WHERE user_id = " + user_id);
        c.close();

        List<ObjectNode> response = new ArrayList<ObjectNode>();
        while(rs.next()) {
            ObjectNode node = Json.newObject();
            node.put("id", rs.getInt("id"));
            node.put("user_id", rs.getLong("user_id"));
            node.put("observed_on", rs.getLong("observed_on"));
            node.put("posted_on", rs.getLong("posted_on"));
            node.put("description", rs.getString("description"));
            node.put("latitude", rs.getDouble("latitude"));
            node.put("longitude", rs.getDouble("longitude"));
            node.put("plot_number", rs.getInt("plot_number"));
            node.put("plot_subnumber", rs.getInt("plot_subnumber"));
            node.put("bed_number", rs.getInt("bed_number"));
            response.add(node);
        }
        ObjectNode wrapper = Json.newObject();
        wrapper.put("observations", Json.toJson(response));
        return ok(wrapper);
    }

    public static Result getByPlotNumber(Integer plot_id) throws SQLException {
        if(plot_id < 1 || plot_id > 72) {
            return badRequest(ResponseWrapper.badRequest("Invalid plot_number"));
        }

        Connection c = Postgres.get();
        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM observations WHERE plot_number = " + plot_id);
        c.close();

        List<ObjectNode> response = new ArrayList<ObjectNode>();
        while(rs.next()) {
            ObjectNode node = Json.newObject();
            node.put("id", rs.getInt("id"));
            node.put("user_id", rs.getInt("user_id"));
            node.put("observed_on", rs.getLong("observed_on"));
            node.put("posted_on", rs.getLong("posted_on"));
            node.put("description", rs.getString("description"));
            node.put("latitude", rs.getDouble("latitude"));
            node.put("longitude", rs.getDouble("longitude"));
            node.put("plot_number", rs.getInt("plot_number"));
            node.put("plot_subnumber", rs.getInt("plot_subnumber"));
            node.put("bed_number", rs.getInt("bed_number"));
            response.add(node);
        }
        ObjectNode wrapper = Json.newObject();
        wrapper.put("observations", Json.toJson(response));
        return ok(wrapper);
    }

    public static Result getByPlotAndBedNumber(Integer plot_id, Integer bed_id) throws SQLException {
        if(plot_id < 1 || plot_id > 72) {
            return badRequest(ResponseWrapper.badRequest("Invalid plot_number"));
        }

        if(bed_id < 1 || bed_id > 42) {
            return badRequest(ResponseWrapper.badRequest("Invalid bed_number"));
        }
        Connection c = Postgres.get();
        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM observations WHERE plot_number = " + plot_id + " AND bed_number = " + bed_id);
        c.close();

        List<ObjectNode> response = new ArrayList<ObjectNode>();
        while(rs.next()) {
            ObjectNode node = Json.newObject();
            node.put("id", rs.getInt("id"));
            node.put("user_id", rs.getInt("user_id"));
            node.put("observed_on", rs.getLong("observed_on"));
            node.put("posted_on", rs.getLong("posted_on"));
            node.put("description", rs.getString("description"));
            node.put("latitude", rs.getDouble("latitude"));
            node.put("longitude", rs.getDouble("longitude"));
            node.put("plot_number", rs.getInt("plot_number"));
            node.put("plot_subnumber", rs.getInt("plot_subnumber"));
            node.put("bed_number", rs.getInt("bed_number"));
            response.add(node);
        }
        ObjectNode wrapper = Json.newObject();
        wrapper.put("observations", Json.toJson(response));
        return ok(wrapper);
    }

    @With(SecurityController.class)
    public static Result deleteByID(Integer id) throws SQLException {
        Integer user_id = null;
        if(Http.Context.current().args.get("user_id") != null) {
            user_id = (Integer)Http.Context.current().args.get("user_id");
        } else {
            user_id = 1;
        }
        Connection c = Postgres.get();
        Statement check_st = c.createStatement();
        ResultSet rs = check_st.executeQuery("SELECT user_id FROM observations WHERE id = " + id);
        if(rs.next()) {
            Integer o_user_id = rs.getInt("user_id");
            if(!user_id.equals(o_user_id)) {
                return unauthorized(ResponseWrapper.unauthorized("You do not own that resource."));
            }
        } else {
            return notFound(ResponseWrapper.notFound("Observation does not exist."));
        }

        Statement st = c.createStatement();
        int result = st.executeUpdate("DELETE FROM observations WHERE id = " + id);
        c.close();
        return ok(ResponseWrapper.success());
    }

    @With(SecurityController.class)
    public static Result testAuth() {
        Logger.debug("user id = " + (Integer)ctx().args.get("user_id"));
        return ok(ResponseWrapper.success());
    }

    @With(UCDavisAuth.class)
    public static Result testAuthDavis() {
        return ok(ResponseWrapper.success());
    }

    @With(SecurityController.class)
    public static Result getUpdateFeed() throws SQLException {
        Connection c = Postgres.get();
        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM observations ORDER BY posted_on DESC LIMIT 15");
        c.close();

        List<ObjectNode> response = new ArrayList<ObjectNode>();
        while(rs.next()) {
            ObjectNode node = Json.newObject();
            node.put("id", rs.getInt("id"));
            node.put("user_id", rs.getInt("user_id"));
            node.put("observed_on", rs.getLong("observed_on"));
            node.put("posted_on", rs.getLong("posted_on"));
            node.put("description", rs.getString("description"));
            node.put("latitude", rs.getDouble("latitude"));
            node.put("longitude", rs.getDouble("longitude"));
            node.put("plot_number", rs.getInt("plot_number"));
            node.put("plot_subnumber", rs.getInt("plot_subnumber"));
            node.put("bed_number", rs.getInt("bed_number"));
            response.add(node);
        }
        ObjectNode wrapper = Json.newObject();
        wrapper.put("update_count", response.size());
        wrapper.put("observations", Json.toJson(response));
        return ok(wrapper);
    }
}
