package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.xml.sax.SAXException;
import models.Member;
import models.ResponseWrapper;
import org.joda.time.DateTime;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import play.Logger;
import play.libs.F;
import play.libs.Json;
import play.libs.WS;
import play.libs.XPath;
import play.mvc.*;
import org.mindrot.jbcrypt.BCrypt;
import util.Postgres;
import util.RandomString;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.BufferedReader;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.StringReader;
import java.sql.*;

public class MembersController extends Controller {

    public static Result addMember() throws SQLException {
        JsonNode json = request().body().asJson();
        Connection c = Postgres.get();
        PreparedStatement st = c.prepareStatement(
                "INSERT INTO members (username, password, joined_on, last_login, email_address, name, type) " +
                        "VALUES (?,?,?,?,?,?,?)"
        );
        if(json.get("username") != null) {
          st.setString(1, json.get("username").asText());
        }
        else { return badRequest(ResponseWrapper.badRequest("You need to submit a username")); }
        if(json.get("password") != null) {
          st.setString(2, BCrypt.hashpw(json.get("password").asText(), BCrypt.gensalt()));
        }
        else { return badRequest(ResponseWrapper.badRequest("You need to submit a password.")); }
        Long now = DateTime.now().getMillis();
        st.setLong(3, now);
        st.setLong(4, now);
        if(json.get("email_address") != null) {
            String email = json.get("email_address").asText();
            st.setString(5,email);
            String email_split[] = email.split("@");
            if(email_split.length != 2) {
                return badRequest(ResponseWrapper.badRequest("Invalid email address."));
            }
            if(email_split[1].contains("ucdavis.edu")) {
                st.setShort(7,(short)1);
            } else {
                st.setShort(7,(short)0);
            }
        } else {
            st.setString(5, null);
            st.setShort(7, (short) 0);
        }
        if(json.get("name") != null) {
            st.setString(6,json.get("name").asText());
        } else {
            st.setString(6,null);
        }
        st.executeUpdate();
        c.close();
        ObjectNode o = Json.newObject();
        o.put("username", json.get("username").asText());
        return ok(o);
    }

    public static Result getMember(String username) throws SQLException {
        Connection c = Postgres.get();
        PreparedStatement st = c.prepareStatement(
          "SELECT id, username, joined_on, name FROM members WHERE id = (SELECT id FROM members WHERE username = ?)");
        st.setString(1, username);
        ResultSet rs = st.executeQuery();
        rs.next();
        c.close();
        ObjectNode o = Json.newObject();
        o.put("id", rs.getInt("id"));
        o.put("username", rs.getString("username"));
        o.put("joined_on", rs.getLong("joined_on"));
        o.put("name", rs.getString("name"));
        return ok(o);
    }

    public static Result login() throws SQLException {
        JsonNode json = request().body().asJson();
        String username = null;
        String password = null;
        if(json.get("username") != null) username = json.get("username").asText();
        else { return badRequest(ResponseWrapper.badRequest("You need to submit a username")); }
        if(json.get("password") != null) password = json.get("password").asText();
        else { return badRequest(ResponseWrapper.badRequest("You need to submit a password")); }

        Connection c = Postgres.get();
        PreparedStatement st = c.prepareStatement(
            "SELECT id, password, current_token FROM members WHERE username = ?"
        );
        st.setString(1, username);
        ResultSet rs = st.executeQuery();
        rs.next();
        if(BCrypt.checkpw(password, rs.getString("password"))) {
            ObjectNode node = Json.newObject();
            String token = RandomString.getRandomString();
            node.put("token", token);
            node.put("authenticated", true);

            Statement delete = c.createStatement();
            delete.executeUpdate("DELETE FROM auth_tokens WHERE token = '" + rs.getString("current_token") + "'");
            Statement st2 = c.createStatement();
            st2.executeUpdate("INSERT INTO auth_tokens (token, user_id, issued, expires) " +
              "VALUES ('" + token + "'," + rs.getInt("id") + "," + DateTime.now().getMillis() + ",0)"
            );
            Statement st3 = c.createStatement();
            st3.executeUpdate("UPDATE members SET current_token = '" + token + "' WHERE id = " + rs.getInt("id"));
            c.close();
            return ok(node);
        } else {
            c.close();
            return unauthorized(ResponseWrapper.unauthorized());
        }
    }

    public static F.Promise<Result> receiveTicket() throws SQLException{
        JsonNode node = request().body().asJson();
        String ticket = node.get("ticket").asText();
        //String ticket = request().body().asFormUrlEncoded().get("ticket")[0];
        F.Promise<WS.Response> response = WS.url("https://cas.ucdavis.edu/cas/serviceValidate")
                .setQueryParameter("service","http://50.116.23.149/index.php")
                .setQueryParameter("ticket",ticket).get();
        return response.map(
                new F.Function<WS.Response, Result>() {
                    public Result apply(WS.Response response) throws SQLException {
                        InputSource source = new InputSource(new StringReader(response.getBody()));

                        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
                        domFactory.setNamespaceAware(true);
                        try {
                            DocumentBuilder builder = domFactory.newDocumentBuilder();
                            Document doc = builder.parse(source);
                            if(doc.getElementsByTagNameNS("http://www.yale.edu/tp/cas", "authenticationSuccess").getLength() < 1) {
                                return unauthorized(ResponseWrapper.unauthorized());
                            } else {
                                Connection c = Postgres.get();
                                Statement st = c.createStatement();

                                String username = doc.getElementsByTagNameNS("http://www.yale.edu/tp/cas", "user").item(0).getTextContent();
                                ResultSet rs = st.executeQuery("SELECT id, current_token FROM members WHERE username = '" + username + "'");

                                Integer id = null;
                                if(rs.next()) {
                                    id = rs.getInt("id");
                                    Statement delete = c.createStatement();
                                    delete.executeUpdate("DELETE FROM auth_tokens WHERE token = '" + rs.getString("current_token") + "'");
                                } else {
                                  ResultSet rs2 = st.executeQuery("INSERT INTO members (username, joined_on, last_login) " +
                                    "VALUES ('" + username + "'," + DateTime.now().getMillis() + "," + DateTime.now().getMillis() + ") RETURNING id"
                                  );
                                  rs2.next();
                                  id = rs2.getInt("id");
                                }
                                ObjectNode node = Json.newObject();
                                String token = RandomString.getRandomString();
                                node.put("token", token);
                                node.put("authenticated", true);

                                Statement st2 = c.createStatement();
                                st2.executeUpdate("INSERT INTO auth_tokens (token, user_id, issued, expires) " +
                                                "VALUES ('" + token + "'," + id + "," + DateTime.now().getMillis() + ",0)"
                                );
                                Statement st3 = c.createStatement();
                                st3.executeUpdate("UPDATE members SET current_token = '" + token + "' WHERE id = " + id);
                                c.close();
                                return ok(node);
                            }
                        } catch (SAXException ex) {
                            return internalServerError(ResponseWrapper.unauthorized());
                        } catch (ParserConfigurationException ex2) {
                            return internalServerError(ResponseWrapper.unauthorized());
                        } catch (IOException ex3) {
                            return internalServerError(ResponseWrapper.unauthorized());
                        }

                        /*Logger.debug("retrieved = " + doc.getElementsByTagNameNS(
                                "http://www.yale.edu/tp/cas", "user").item(0).getTextContent());
                        Logger.debug("error = " + doc.getElementsByTagNameNS(
                                "http://www.yale.edu/tp/cas", "authenticationFailure").item(0).getTextContent());
                        Logger.debug("testing");*/

                        //Logger.debug(XPath.selectText("//cas:authenticationFailure", response.getBody()));
                        //Logger.debug("testing = " + XPath.selectText("//cas:authenticationFailure", response.asXml()));
                        //Logger.debug(response.asXml().getElementById("cas:user").toString());
                        //return ok();
                    }
                }
        );
    }

    public static Result ticket() {
        Logger.debug("ticket = " + request().getQueryString("ticket"));
        return ok();
    }
}
