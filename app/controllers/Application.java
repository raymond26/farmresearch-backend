package controllers;

import play.*;
import play.mvc.*;

import util.Postgres;
import views.html.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render("Your new application is ready."));
    }

    public static Result importPlotSpecs() throws IOException, SQLException, InterruptedException {
        /*List<String> lines = Files.readAllLines(Paths.get("/Users/raymondlau/Classes","plot_system_name_new.csv"));
        Connection c = Postgres.get();
        PreparedStatement ps = c.prepareStatement(
                "INSERT INTO plot_specs VALUES (?,?,?,?,?,?,?,?,?)");
        for(String line : lines) {
            String data[] = line.split(",");
            String plot_numbers[] = data[0].split("-");
            String system_name = data[1];
            Integer start = null;
            if(data[2].equals("A")) {
                start = 0;
            } else if(data[2].equals("B")) {
                start = 1;
            }
            String irrigation = data[3];
            String nitrogen_1 = data[4];
            String nitrogen_2 = data[5];
            String crop_1 = data[6];
            String crop_2 = data[7];

            ps.setString(1, plot_numbers[0]);
            ps.setString(2, plot_numbers[1]);
            ps.setString(3, system_name);
            ps.setInt(4, start);
            ps.setString(5, irrigation);
            ps.setString(6, nitrogen_1);
            ps.setString(7, nitrogen_2);
            ps.setString(8, crop_1);
            ps.setString(9, crop_2);
            ps.executeUpdate();

            Thread.sleep(50);
        }
        c.close();*/

        return ok();
    }

}
