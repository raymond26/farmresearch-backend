package controllers
import play.api.mvc._
import scala.concurrent.Future
import play.api.Play.current
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.Logger

object PhotosController extends Controller {

  def acceptPhoto = Action.async { request =>
    request.body.asMultipartFormData match {
      case Some(multipart) =>
        Logger.debug("no. files received = " + multipart.files.size + " and files = " + multipart.files(0).filename + " and contentype = " + multipart.files(0).contentType)
      case None =>
        Logger.debug("No Multipart")
    }

    Future(Ok(""))
  }

}
