# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table auth_token (
  token                     varchar(255) not null,
  username                  varchar(255),
  issued_on                 timestamp,
  expires_on                timestamp,
  constraint pk_auth_token primary key (token))
;

create table member (
  username                  varchar(255) not null,
  password                  varchar(255),
  joined_on                 timestamp,
  last_login                timestamp,
  email_address             varchar(255),
  constraint pk_member primary key (username))
;

create table observation (
  id                        bigint not null,
  observed_on               timestamp,
  posted_on                 timestamp,
  description               varchar(255),
  latitude                  bigint,
  longitude                 bigint,
  plot_number               integer,
  bed_number                integer,
  constraint pk_observation primary key (id))
;

create table observation_photo (
  id                        bigint not null,
  filename                  varchar(255),
  location_on_disk          varchar(255),
  o_id                      bigint,
  constraint pk_observation_photo primary key (id))
;

create sequence auth_token_seq;

create sequence member_seq;

create sequence observation_seq;

create sequence observation_photo_seq;




# --- !Downs

drop table if exists auth_token cascade;

drop table if exists member cascade;

drop table if exists observation cascade;

drop table if exists observation_photo cascade;

drop sequence if exists auth_token_seq;

drop sequence if exists member_seq;

drop sequence if exists observation_seq;

drop sequence if exists observation_photo_seq;

